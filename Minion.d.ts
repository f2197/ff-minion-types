//#region CONSTANTS
declare const ActionList: MinionActionList;
declare const MinionPlayer: PlayerUnit;
declare const MinionTarget: TargetUnit;
//#endregion

//#region ACR
declare namespace ACR {
    export function DrawModKey(): void;
    export function RunningProfile(): void;
    export function IsFacing(): void;
    export function RemoveMiniButton(): void;
    export const ClicksDisplay: [
        "None",
        "PAUSE",
        "SPACEBAR",
        "PAGE UP",
        "PAGE DOWN",
        "END",
        "HOME",
        "LEFT ARROW",
        "UP ARROW",
        "RIGHT ARROW",
        "DOWN ARROW",
        "PRINT",
        "INS",
        "DEL",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
        "NUM 0",
        "NUM 1",
        "NUM 2",
        "NUM 3",
        "NUM 4",
        "NUM 5",
        "NUM 6",
        "NUM 7",
        "NUM 8",
        "NUM 9",
        "Separator",
        "Subtract",
        "Decimal",
        "Divide",
        "F1",
        "F2",
        "F3",
        "F4",
        "F5",
        "F6",
        "F7",
        "F8",
        "F9",
        "F10",
        "F11",
        "F12",
    ]
    export const ClicksMap: [
        1,
        19,
        32,
        33,
        34,
        35,
        36,
        37,
        38,
        39,
        40,
        42,
        45,
        46,
        48,
        49,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
        65,
        66,
        67,
        68,
        69,
        70,
        71,
        72,
        73,
        74,
        75,
        76,
        77,
        78,
        79,
        80,
        81,
        82,
        83,
        84,
        85,
        86,
        87,
        88,
        89,
        90,
        96,
        97,
        98,
        99,
        100,
        101,
        102,
        103,
        104,
        105,
        108,
        109,
        110,
        111,
        112,
        113,
        114,
        115,
        116,
        117,
        118,
        119,
        120,
        121,
        122,
        123,
    ]
    export function HasBuff(): void;
    export function SetGUIVar(): void;
    export const API: {
    }
    export function CastOriginal(): void;
    export function OnGameUpdateOriginal(): void;
    export function GetBuffX(): void;
    export function CraftOriginal(): void;
    export function MissingBuffX(): void;
    export function OpenProfileOptions(): void;
    export function OpenGUI(): void;
    export function DrawMouseKey(): void;
    export function GatherOriginal(): void;
    export function CreateDisplayMap(): void;
    export function GetSetting(this: void, id: string, TODObool: boolean): any;
    export const ModifierClicksDisplay: [
        "None",
        "Shift",
        "Control",
    ]
    export function OnUpdateOriginal(): void;
    export function DrawClickKey(): void;
    export const ModifierClicksMap: [
        1,
        16,
        17,
    ]
    export const ModifierClicks: {
        [0]: "None",
        [16]: "Shift",
        [17]: "Control",
    }
    export const MouseClicksDisplay: {
        [1]: "Left Button",
        [2]: "Right Button",
        [3]: "Middle Button",
        [4]: "Middle Mouse",
        [5]: "Mouse 4",
        [6]: "Mouse 5",
        [0]: "None",
    }
    export const Clicks: {
        [1]: "None",
        [19]: "PAUSE",
        [32]: "SPACEBAR",
        [33]: "PAGE UP",
        [34]: "PAGE DOWN",
        [35]: "END",
        [36]: "HOME",
        [37]: "LEFT ARROW",
        [38]: "UP ARROW",
        [39]: "RIGHT ARROW",
        [40]: "DOWN ARROW",
        [42]: "PRINT",
        [45]: "INS",
        [46]: "DEL",
        [48]: "0",
        [49]: "1",
        [50]: "2",
        [51]: "3",
        [52]: "4",
        [53]: "5",
        [54]: "6",
        [55]: "7",
        [56]: "8",
        [57]: "9",
        [65]: "A",
        [66]: "B",
        [67]: "C",
        [68]: "D",
        [69]: "E",
        [70]: "F",
        [71]: "G",
        [72]: "H",
        [73]: "I",
        [74]: "J",
        [75]: "K",
        [76]: "L",
        [77]: "M",
        [78]: "N",
        [79]: "O",
        [80]: "P",
        [81]: "Q",
        [82]: "R",
        [83]: "S",
        [84]: "T",
        [85]: "U",
        [86]: "V",
        [87]: "W",
        [88]: "X",
        [89]: "Y",
        [90]: "Z",
        [96]: "NUM 0",
        [97]: "NUM 1",
        [98]: "NUM 2",
        [99]: "NUM 3",
        [100]: "NUM 4",
        [101]: "NUM 5",
        [102]: "NUM 6",
        [103]: "NUM 7",
        [104]: "NUM 8",
        [105]: "NUM 9",
        [108]: "Separator",
        [109]: "Subtract",
        [110]: "Decimal",
        [111]: "Divide",
        [112]: "F1",
        [113]: "F2",
        [114]: "F3",
        [115]: "F4",
        [116]: "F5",
        [117]: "F6",
        [118]: "F7",
        [119]: "F8",
        [120]: "F9",
        [121]: "F10",
        [122]: "F11",
        [123]: "F12",
    }
    // newVal,varName,doSave,forceSave
    /** Automatically updates an ACR variable 
     * 
     * @param value The value to set the setting to when the GUI updates
     * @param name The name of the setting
     * @param saveNow Should it try to save at runtime
     * @param forceSave Should it force the save at runtime
     */
    export function GUIVarUpdate(value: string | number | boolean, name: string, saveNow?: boolean, forceSave?: boolean): void;
    /** Insert a private profile into ACR programatically.
     * @param {string} fileInfo The data from the profile.
     * @param {string} alias The name to be given to the profile.
     * @example
     * const [fileData, e] = persistence.load(myFilePath);
     * if (fileData and ValidTable(fileData)) {
     *      ACR.AddPrivateProfile(fileData, alias);
     * }
     */
    export function AddPrivateProfile(this: void, fileInfo: object, alias: string): void;
    export function GetGeneralIcon(): void;
    export function MissingBuff(): void;
    export const MouseClicksMap: {
        [0]: -1,
        [1]: 0,
        [2]: 1,
        [3]: 2,
        [4]: 3,
        [5]: 4,
        [6]: 5,
    }
    export function DrawAddonHeader(): void;
    export function HasThrottle(): void;
    export function DrawAddonFooter(): void;
    export function AddMiniButton(): void;
    export function AddKeyCode(): void;
    export function ForceEnable(): void;
    export function AddThrottle(): void;
    export function HasBuffX(): void;
}
//#endregion
//#region Generic
declare interface I2DCoordinate {
    x: number,
    y: number,
    h?: number
}
declare interface I3DCoordinate extends I2DCoordinate {
    z: number,
}
declare interface IMeshPosition extends I3DCoordinate {
    distance: number;
    meshdistance: number;
}
declare interface ICastingTargets {
    // index, CastID
    [index: number]: number;
}
declare interface ICastingInfo {
    ptr: number;
    castingid: number;
    casttime: number;
    castingtargetcount: number;
    castinginterruptible: boolean;
    castingtargets: ICastingTargets[] | undefined;
    lastcastid: number;
    timesincecast: number;
    channelingid: number;
    channeltargetid: number;
    channeltime: number;
}
//#endregion
//#region UI


//#region UI Models
declare interface IUIManagerSubMember {
    id: string,
    name: string,
    onClick?: () => void,
    tooltip?: string,
    texture?: string,
}
declare interface IUIManagerMember {
    id: string,
    name: string,
    onClick?: () => void,
    tooltip?: string,
    texture?: string,
    sort?: boolean
}
declare interface IUIManagerComponent {
    name: string,
    id: string,
    expanded: boolean,
    members?: IUIManagerMember[];
    texture?: string
}
declare namespace ml_gui {
    export namespace ui_mgr {
        /** Add a submember to an existing member object
         * @param {IUIManagerSubMember} subMember The submember payload, excludes sort and will sort alphabetically if the parent has a sort set to true.
         * @param {string} componentId The unique identifier for this component
         * @param {string} memberId The identifier of the parent.
         * @example
        * ml_gui.ui_mgr:AddSubMember({ 
        *       id = "FFXIVMINION##DEV_1", 
        *       name = "DevA", 
        *       onClick = function() Dev.GUI.open = not Dev.GUI.open end, 
        *       tooltip = "Open the Dev monitor."
         * },"FFXIVMINION##MENU_HEADER","FFXIVMINION##MENU_DEV5")
         */
        export function AddSubMember(subMember: IUIManagerSubMember, componentId: string, ...memberId: string[]): void;
        /** Adds a member to the parent component
         * 
         * @param {IUIManagerMember} member The member payload
         * @param {string} compenentId The unique identifier for this component
         * @example
         * ml_gui.ui_mgr:AddMember({ 
         *      id = "FFXIVMINION##MENU_DEV1", 
         *      name = "Dev1", 
         *      onClick = function() Dev.GUI.open = not Dev.GUI.open end, 
         *      tooltip = "Open the Dev monitor."},
         * "FFXIVMINION##MENU_HEADER")
         */
        export function AddMember(member: IUIManagerMember, compenentId: string): void;
        /** Creates a component
         * 
         * @param {IUIManagerComponent} component The member payload
         * @example
         * const MainMenu = { 
         *      header = {
         *          id = "FFXIVMINION##MENU_HEADER", 
         *          expanded = false, 
         *          name = "FFXIVMinion", 
         *          texture = GetStartupPath().."\\GUI\\UI_Textures\\ffxiv_shiny.png"" 
         *      },
         *      members = [];
         * };
         * ml_gui.ui_mgr:AddComponent(MainMenu);
         */
        export function AddComponent(component: IUIManagerComponent): void;
    }
}
//#endregion
interface ITabTitle {
    a: number,
    b: number,
    g: number,
    name: string,
    r: number
}
interface ITabs {
    options: {};
    events: {};
    tabs: {
        hovered: ITabTitle,
        ishovered: boolean,
        isselected: boolean,
        normal: ITabTitle,
        selected: ITabTitle
    }[]

}
/** Creates UI Objects for each tab specified in {tabs}
 * @param {string} tabs comma seperated string of tab names
 * @returns {any} - TODO: Update this
 * @example const myTabs = GUI_CreateTabs("Page 1, Page 2, Page 3");
 */
declare function GUI_CreateTabs(this: void, tabs: string): ITabs
/** Grabs the active tab from the tab object reference (NYI)
 * @param tabs 
 * @returns {[number, string]} - The Tab Index and Name
 * @example const [tabIndex, tabName] = GUI_DrawTabs(myTabs)
 * @tupleReturn
 */
declare function GUI_DrawTabs(this: void, tab: any): [number, string]

// TODO: Document these as I use them!
declare namespace GUI {
    export const Col_Border = 5;
    export const Col_BorderShadow = 6;
    export const Col_Button = 21;
    export const Col_ButtonActive = 23;
    export const Col_ButtonHovered = 22;
    export const Col_CheckMark = 18;
    export const Col_ChildWindowBg = 3;
    export const Col_CloseButton = 0;
    export const Col_CloseButtonActive = 0;
    export const Col_CloseButtonHovered = 0;
    export const Col_Column = 27;
    export const Col_ColumnActive = 29;
    export const Col_ColumnHovered = 28;
    export const Col_ComboBg = 0;
    export const Col_DragDropTarget = 38;
    export const Col_FrameBg = 7;
    export const Col_FrameBgActive = 9;
    export const Col_FrameBgHovered = 8;
    export const Col_Header = 24;
    export const Col_HeaderActive = 26;
    export const Col_HeaderHovered = 25;
    export const Col_MenuBarBg = 13;
    export const Col_ModalWindowDarkening = 42;
    export const Col_NavHighlight = 39;
    export const Col_NavWindowingHighlight = 40;
    export const Col_PlotHistogram = 35;
    export const Col_PlotHistogramHovered = 36;
    export const Col_PlotLines = 33;
    export const Col_PlotLinesHovered = 34;
    export const Col_ResizeGrip = 30;
    export const Col_ResizeGripActive = 32;
    export const Col_ResizeGripHovered = 31;
    export const Col_ScrollbarBg = 14;
    export const Col_ScrollbarGrab = 15;
    export const Col_ScrollbarGrabActive = 17;
    export const Col_ScrollbarGrabHovered = 16;
    export const Col_SliderGrab = 19;
    export const Col_SliderGrabActive = 20;
    export const Col_Text = 0;
    export const Col_TextDisabled = 1;
    export const Col_TextSelectedBg = 37;
    export const Col_TitleBg = 10;
    export const Col_TitleBgActive = 11;
    export const Col_TitleBgCollapsed = 12;
    export const Col_TooltipBg = 4;
    export const Col_WindowBg = 2;
    export const ColorEditMode_AlphaBar = 65536;
    export const ColorEditMode_AlphaPreview = 131072;
    export const ColorEditMode_AlphaPreviewHalf = 262144;
    export const ColorEditMode_Float = 16777216;
    export const ColorEditMode_HDR = 524288;
    export const ColorEditMode_HEX = 4194304;
    export const ColorEditMode_HSV = 2097152;
    export const ColorEditMode_NoAlpha = 2;
    export const ColorEditMode_NoInputs = 32;
    export const ColorEditMode_NoLabel = 128;
    export const ColorEditMode_NoOptions = 8;
    export const ColorEditMode_NoSidePreview = 256;
    export const ColorEditMode_NoSmallPreview = 16;
    export const ColorEditMode_NoTooltip = 64;
    export const ColorEditMode_PickerHueBar = 33554432;
    export const ColorEditMode_PickerHueWheel = 67108864;
    export const ColorEditMode_RGB = 1048576;
    export const ColorEditMode_Uint8 = 8388608;
    export const ColorEditMode_UserSelect = 0;
    export const ColorEditMode_UserSelectShowButton = 0;
    export const ComboFlags_HeightLarge = 8;
    export const ComboFlags_HeightLargest = 16;
    export const ComboFlags_HeightRegular = 4;
    export const ComboFlags_HeightSmall = 2;
    export const ComboFlags_NoArrowButton = 32;
    export const ComboFlags_NoPreview = 64;
    export const ComboFlags_PopupAlignLeft = 1;
    export const Dir_Down = 3;
    export const Dir_Left = 0;
    export const Dir_Right = 1;
    export const Dir_Up = 2;
    export const DrawCornerFlag_BottomLeft = 4;
    export const DrawCornerFlag_BottomRight = 8;
    export const DrawCornerFlag_TopLeft = 1;
    export const DrawCornerFlag_TopRight = 2;
    export const FocusedFlags_AnyWindow = 4;
    export const FocusedFlags_ChildWindows = 1;
    export const FocusedFlags_RootWindow = 2;
    export const HoveredFlags_AllowWhenBlockedByActiveItem = 32;
    export const HoveredFlags_AllowWhenBlockedByPopup = 8;
    export const HoveredFlags_AllowWhenOverlapped = 64;
    export const HoveredFlags_AnyWindow = 4;
    export const HoveredFlags_ChildWindows = 1;
    export const HoveredFlags_Default = 0;
    export const HoveredFlags_RootWindow = 2;
    export const InputTextFlags_AllowTabInput = 1024;
    export const InputTextFlags_AlwaysInsertMode = 8192;
    export const InputTextFlags_AutoSelectAll = 16;
    export const InputTextFlags_CallbackAlways = 256;
    export const InputTextFlags_CallbackCharFilter = 512;
    export const InputTextFlags_CallbackCompletion = 64;
    export const InputTextFlags_CallbackHistory = 128;
    export const InputTextFlags_CharsDecimal = 1;
    export const InputTextFlags_CharsHexadecimal = 2;
    export const InputTextFlags_CharsNoBlank = 8;
    export const InputTextFlags_CharsScientific = 131072;
    export const InputTextFlags_CharsUppercase = 4;
    export const InputTextFlags_CtrlEnterForNewLine = 2048;
    export const InputTextFlags_EnterReturnsTrue = 32;
    export const InputTextFlags_NoHorizontalScroll = 4096;
    export const InputTextFlags_NoUndoRedo = 65536;
    export const InputTextFlags_Password = 32768;
    export const InputTextFlags_ReadOnly = 16384;
    export const MouseCursor_Arrow = 0;
    export const MouseCursor_ResizeEW = 4;
    export const MouseCursor_ResizeNESW = 5;
    export const MouseCursor_ResizeNS = 3;
    export const MouseCursor_TextInput = 1;
    export const SelectableFlags_AllowDoubleClick = 4;
    export const SelectableFlags_DontClosePopups = 1;
    export const SelectableFlags_SpanAllColumns = 2;
    export const SetCond_Always = 1;
    export const SetCond_Appearing = 8;
    export const SetCond_FirstUseEver = 4;
    export const SetCond_Once = 2;
    export const StyleVar_Alpha = 0;
    export const StyleVar_ButtonTextAlign = 20;
    export const StyleVar_ChildBorderSize = 7;
    export const StyleVar_ChildWindowRounding = 6;
    export const StyleVar_FrameBorderSize = 12;
    export const StyleVar_FramePadding = 10;
    export const StyleVar_FrameRounding = 11;
    export const StyleVar_GrabMinSize = 18;
    export const StyleVar_GrabRounding = 19;
    export const StyleVar_IndentSpacing = 15;
    export const StyleVar_ItemInnerSpacing = 14;
    export const StyleVar_ItemSpacing = 13;
    export const StyleVar_PopupBorderSize = 9;
    export const StyleVar_PopupRounding = 8;
    export const StyleVar_ScrollbarRounding = 17;
    export const StyleVar_ScrollbarSize = 16;
    export const StyleVar_WindowBorderSize = 3;
    export const StyleVar_WindowMinSize = 4;
    export const StyleVar_WindowPadding = 1;
    export const StyleVar_WindowRounding = 2;
    export const StyleVar_WindowTitleAlign = 5;
    export const TreeNodeFlags_AllowItemOverlap = 4;
    export const TreeNodeFlags_Bullet = 512;
    export const TreeNodeFlags_CollapsingHeader = 26;
    export const TreeNodeFlags_FramePadding = 1024;
    export const TreeNodeFlags_Framed = 2;
    export const TreeNodeFlags_Leaf = 256;
    export const TreeNodeFlags_NavLeftJumpsBackHere = 8192;
    export const TreeNodeFlags_NoAutoOpenOnLog = 16;
    export const TreeNodeFlags_NoTreePushOnOpen = 8;
    export const TreeNodeFlags_OpenOnArrow = 128;
    export const TreeNodeFlags_OpenOnDoubleClick = 64;
    export const TreeNodeFlags_SDefaultOpen = 32;
    export const TreeNodeFlags_Selected = 1;
    export const WindowFlags_AlwaysAutoResize = 64;
    export const WindowFlags_AlwaysUseWindowPadding = 65536;
    export const WindowFlags_ForceHorizontalScrollbar = 32768;
    export const WindowFlags_ForceVerticalScrollbar = 16384;
    export const WindowFlags_HorizontalScrollbar = 2048;
    export const WindowFlags_MenuBar = 1024;
    export const WindowFlags_NoBringToFrontOnFocus = 8192;
    export const WindowFlags_NoCollapse = 32;
    export const WindowFlags_NoFocusOnAppearing = 4096;
    export const WindowFlags_NoInputs = 512;
    export const WindowFlags_NoMove = 4;
    export const WindowFlags_NoResize = 2;
    export const WindowFlags_NoSavedSettings = 256;
    export const WindowFlags_NoScrollWithMouse = 16;
    export const WindowFlags_NoScrollbar = 8;
    export const WindowFlags_NoTitleBar = 1;
    export const WindowFlags_ShowBorders = 0;
    export function IsMouseDoubleClicked(): void;
    /** Required for every GUI.BeginChild() regardless if the child is open or not, this is the end of the contextual execution for the active child
    * @example
    * GUI.BeginChild("Main Window", 100, 100, true);
    *   ... Do stuff that will be within a this child
    * GUI.End();
    */
    export function EndChild(): void;
    export function PushID(): void;
    /** Required for every GUI.Begin() regardless if the window is open or not, this is the end of the contextual execution for this active GUI
     * @example
     * const [visible, open] = GUI.Begin("Main Window");
     * ... do stuff with the UI such as adding checkbxoes, text, etc 
     * GUI.End();
     */
    export function End(): void;
    export function AddCircle(): void;
    /**
     * Set the next window to the front-most (z axis) window.
     * @example
     * GUI.SetNextWindowFocus();
     * This window will be front most, above all other windows on the z axis.
     * const [visible, open] = GUI.Begin("Main Window");
     * ... do stuff with the UI 
     * GUI.End();
     */
    export function SetNextWindowFocus(): void;
    export function EndCombo(): void;
    export function ListBoxFooter(): void;
    export function BeginMainMenuBar(): void;
    /** Get the current content region that is available, sugar for: GetContentRegionMax() - GetCursorPos()
     * @returns [0]: number, [1]: number
     * @example const [width, height] = GUI.GetContentRegionAvail(); 
     * @tupleReturn
     */
    export function GetContentRegionAvail(): [number, number];
    export function SetStyle(): void;
    export function IsAnyItemActive(): void;
    export function EndPopup(): void;
    export function SetScrollHere(): void;
    export function MenuItem(): void;
    export function ColorPicker4(): void;
    export function ColorConvertRGBtoHSV(): void;
    export function SetColumnOffset(): void;
    export function InputInt2(): void;
    export function SliderFloat2(): void;
    export function GetMousePos(): void;
    /** Removes the item width set from this context
     * @example 
     * GUI.PushItemWidth(150)
     * // This tree will be of width 150
     * if (GUI.TreeNode("MyTreeNode")) {
     *     GUI.BulletText("MyBulletText);
     * }
     * // Stop setting items to previously pushed width.
     * GUI.PopItemWidth()
     */
    export function PopItemWidth(): void;
    /**
     * Set the position of the current window to x, y
     * @param name The specific window to set
     * @param x The x position of the window to set
     * @param y The y position of the window to set
     * @param conditionFlags The SetCond flags to set
     * @example
     * GUI.Begin("MyCoolWindow", MyCoolWindow.GUI.Open);
     * // Saved values, set on first use that session
     * GUI.SetWindowPos(MyCoolWindow.GUI.x, MyCoolWindow.GUI.y, SetCond_FirstUseEver);
     * GUI.End();
     */
    export function SetWindowPos(x: number, y: number, conditionFlags?: number): void;
    export function SetWindowPos(name: string, x: number, y: number, conditionFlags?: number): void;
    export function LogToClipboard(): void;
    export function BeginTooltip(): void;
    export function IsMouseClicked(): void;
    export function IsMouseDragging(): void;
    export function ArrowButton(): void;
    export function ProgressBar(): void;
    export function GetColorU32(): void;
    export function IsKeyDown(): void;
    export function IsItemVisible(): void;
    export function LogButtons(): void;
    export function Keybind(): void;
    export function TextDisabled(): void;
    export function ColorButton(): void;
    export function IsItemClicked(): void;
    export function Separator(): void;
    export function InputInt3(): void;
    export function IsMouseDown(): void;
    export function DragIntRange2(): void;
    /**
     * Get the width of the current content region
     * @returns The width as a number
     * @example const width = GUI.GetWindowContentRegionWidth();
     */
    export function GetWindowContentRegionWidth(): number;
    export function CloseCurrentPopup(): void;
    export function GetTextLineHeight(): void;
    export function AddText(): void;
    export function ShowConsole(): void;
    export function ResetMouseDragDelta(): void;
    /** Checks to see if the current Window context is appearing
     * 
     * @returns {boolean} Whether or not the current window context is appearing on the screen
     * @example if (GUI.IsWindowAppearing()) {
     *      .. do stuff
     * }
     */
    export function IsWindowAppearing(): boolean;
    export function GetScrollMaxY(): void;
    export function PopStyleColor(): void;
    export function BeginMenu(): void;
    export function GetFrameHeightWithSpacing(): void;
    export function BeginPopupContextVoid(): void;
    export function VSliderFloat(): void;
    export function GetFrameCount(): void;
    export function GetTextLineHeightWithSpacing(): void;
    export function SetWindowFontScale(): void;
    /** Pops the active tree from the context, allowing for next context
     * @example 
     * if (GUI.TreeNode("MyNode")) {
     *     GUI.BulletText("My Bullet Text");
     *     GUI.SameLine(200);
     *     GUI.InputText("MyTextIdentifier", "MyTextValue");
     *     GUI.TreePop();
     * }
     */
    export function TreePop(): void;
    export function GetColumnOffset(): void;
    /** Sets the next tree to be created within this GUI Context to be opened by default
     * @param {boolean} TODObool - not sure what this does yet, maybe sets the Open prop of the tree to true?
     * @param {number} condition - The condition enum for when this should occur
     * @example 
     * GUI.SetNextTreeNodeOpened(true, GUI.SetCond_Always);
     * if (GUI.TreeNode("MyTreeNode")) { 
     *      this will immediately fire, as we set it to automatically be open upon creation 
     * }
     */
    export function SetNextTreeNodeOpened(TODObool: boolean, condition?: number): void;
    export function GetStyle(): void;
    /** Input a test with Bullet styling
     * @param {string} value The value to show in the test bullet
     * @returns {void} - No return but it does create a contextual bullet text in the active UI.
     * @example BulletText.("MyText") will create a text with bullet styling with text "MyText"
     */
    export function BulletText(value: string): void;
    export function ColorPicker3(): void;
    export function GetCursorPosX(): void;
    export function IsPopupOpen(): void;
    export function Selectable(identifier: any, value: any): any;
    export function GetKeyIndex(): void;
    export function GetColumnsCount(): void;
    export function TreePush(): void;
    export function SetCursorPosX(): void;
    export function InputFloat4(): void;
    export function SetCursorScreenPos(): void;
    export function IsKeyPressed(): void;
    export function Indent(): void;
    /**
     * 
     * @param {number} width - The width of the next item in the GUI context
     * 
     */
    export function PushItemWidth(width: number): void;
    export function PushStyleVar(styleVar: number, num1: number, num2: number): void;
    export function LogToTTY(): void;
    export function ListBox(): void;
    export function IsItemActive(): void;
    export function BeginCombo(): void;
    export function SliderInt4(): void;
    export function Bullet(): void;
    export function InputInt4(): void;
    export function BeginChildFrame(): void;
    export function CollapsingHeader(): void;
    /** Get the current content boundaries (typically window boundaries including scrolling, or current column boundaries), in window coordinates
     * @returns [0]: number, [1]: number
     * @example const [width, height] = GUI.GetContentRegionMax(); 
     * @tupleReturn
     */
    export function GetContentRegionMax(): [number, number];
    export function IsItemHovered(): boolean;
    export function IsKeyReleased(): void;
    export function SmallButton(): void;
    export function TextUnformatted(): void;
    export function PushStyleColor(): void;
    export function PushTextWrapPos(): void;
    export function PushAllowKeyboardFocus(): void;
    /**
     * Sets the **next** window to the following position, must be called **before** GUI.Begin
     * @param width The width of the window
     * @param height The height of the window
     * @param conditionFlags the SetCond flags to be set
     * @example
     * GUI.SetNextWindowSize(300, 300, GUI.SetCond_FirstUseEver)
     * const [vis, op] = GUI.Begin("MyWindow", MyWindow.GUI.Open);
     * ... do stuff with the windows
     * GUI.End();
     */
    export function SetNextWindowSize(width: number, height: number, conditionFlags?: number): void;
    export function EndMainMenuBar(): void;
    export function GetColumnIndex(): void;
    export function IsAnyItemHovered(): void;
    export function ColorEdit3(): void;
    export function InputTextEditor(identifier: string, value: string): string;
    export function IsRectVisible(): void;
    export function BeginGroup(): void;
    export function GetTime(): void;
    export function GetItemRectSize(): void;
    export function IsMouseHoveringAnyWindow(): void;
    export function GetColumnWidth(): void;
    /** Set the current or specific window width and height
     * @param name The specific window to adjust
     * @param width The x axis size, set to 0 to leave automatic.
     * @param height The y axis size, set to 0 to leave automatic.
     * One of these two value so need to be set above 0, the other will automatically adjust if left at 0. 
     * @param conditionFlags The SetCond flags to set.
     * @example
     * GUI.Begin("MyCoolWindow", MyCoolWindow.GUI.Open);
     * GUI.SetWindowSize(600, 0, SetCond_FirstUseEver);
     * GUI.End();
     */
    export function SetWindowSize(width: number, height: number, conditionFlags?: number): void;
    export function SetWindowSize(name: string, width: number, height: number, conditionFlags?: number): void;
    export function ImageButton(): void;
    /** Get the height and width of the current window 
     * @returns [0]: number, [1]: number
     * @example
     * const [width, height] = GUI.GetWindowSize();
     * MyWindow.GUI.width = width;
     * MyWindow.GUI.height = height;
     * @tupleReturn
     */
    export function GetWindowSize(): [number, number];
    export function TextColored(): void;
    export function SetColumnWidth(): void;
    export function BeginPopup(): void;
    export function IsMouseHoveringRect(): void;
    export function GetWindowContentRegionMax(): void;
    export function AlignFirstTextHeightToWidgets(): void;
    export function ListBoxHeader(): void;
    /** Check if the tree node is collapsed or not
     * @param {string} identifier - The identifier of the tree node 
     * @returns {boolean} Whether or not the tree node is not collapsed
     * @example 
     * if (GUI.TreeNode("AddonControls")) { 
     *      do something according to the node being visible 
     * }
     */
    export function TreeNode(identifier: string): boolean;
    export function GetMousePosOnOpeningCurrentPopup(): void;

    /** Creates a gap between items in a GUI context.
     * @param {number} dist The distance between items in the UI on the same line (pixels, x coord)
     * @returns {void} - but contextually will create a gap between items of {dist}.
     * @example GUI.SameLine(200) will put 200 pixels before the next item on the same line 
     */
    export function SameLine(dist: number): void;
    export function SliderInt2(): void;
    export function ComboKey(): void;
    export function NewLine(): void;
    /** Creates a child GUI context for all following GUI function execution
     * 
     * @param {string} name The unique identifier of the child
     * @param {number} sizeX The X size of the child
     * @param {number} sizeY The Y Size of the child
     * @param {boolean} border Whether or not to enable the border
     * @param {number} windowFlags The flags you want added to the window as an integer
     * @example
     *  GUI.BeginChild("##style-switch", 35, 35, false);
     *	GUI.Text("");
     *	GUI.Image(ml_global_information.GetMainIcon(), 14, 14);
     *	if (GUI.IsItemHovered()) {
     *		if (GUI.IsMouseClicked(0)) {
     *			if (ml_global_information.drawMode == 1) {
     *				ml_global_information.drawMode = 0;
     *          } else {
     *				ml_global_information.drawMode = 1;
     *          }
     *		}
     *	}
     *  GUI.EndChild();
     */
    export function BeginChild(name: string, sizeX?: number, sizeY?: number, border?: boolean, windowFlags?: number): void;
    export function IsPosHoveringAnyWindow(): void;
    export function Unindent(): void;
    export function BeginPopupModal(): void;
    export function PopStyleVar(): void;
    export function GetCursorPos(): void;
    export function EndTooltip(): void;
    /** Adds an integer to the current GUI Context
     * @param {string} identifier - The identifier of the int
     * @param {number} value - The default value
     * @param {number} step - The minimum step between values
     * @param {number} stepFast - TODO: not sure what this does
     * @param {number} inputFlags - the InputText flags integer  to use
     * @returns number, the value of the intput. boolean, whether or not it was changed
     * @example GUI.BulletText("MyNumber"); GUI.SameLine(100); GUI.InputInt("MyInId", 1);
     * @tupleReturn
     */
    export function InputInt(identifier: string, value: number, step?: number, stepFast?: number, inputFlags?: number): [number, boolean]
    /** Returns the current minimum content boundaries, typically 0, 0 - Scroll.
     * @return [0]: number, [1]: number
     * @example const [w, h] = GUI.GetWindowContentRegionMin();
     * @tupleReturn
     */
    export function GetWindowContentRegionMin(): [number, number];
    export function Spacing(): void;
    export function IsRootWindowOrAnyChildFocused(): void;
    export function EndGroup(): void;
    /** Opens the test window that demonstrates UI features
     * @param {boolean} isOpen Whether or not the window should be open
     */
    export function ShowTestWindow(isOpen: boolean): boolean;
    export function IsMouseReleased(): void;
    export function GetMouseCursor(): void;
    export function DragInt(): void;
    export function GetScrollMaxX(): void;
    export function ShowMetricsWindow(): void;
    /** Check whether the current window context is focused based on the current flags set
     * 
     * @param {number} focusedFlags the FocusedFlags integer you want checked on this window
     * @returns {boolean} Whether or not the window is focused with the set flags
     * @example
     * if (GUI.IsWindowFocused(GUI.FocusedFlags_RootWindow)) {
     *  ... do stuff with the root window
     * }
     */
    export function IsWindowFocused(focusedFlags?: number): boolean;
    export function DragFloatRange2(): void;
    export function AddRectFilled(): void;
    export function SetScrollY(): void;
    export function AddLine(): void;
    /** Adds a Separator line to the current GUI context
     * @example Creates a simple 3 column with Separators
     * GUI.Separator()
     * GUI.Columns(3, "MyIdentifier", true);
     * GUI.Text("Text1"); GUI.NextColumn();
     * GUI.Text("Text2"); GUI.NextColumn();
     * GUI.Text("Text3"); GUI.NextColumn();
     * GUI.Separator()
     */
    export function Separator(): void
    export function SetMouseCursor(): void;
    /** Sets the current column to the next column in the context
     * @example Creates a simple 3 column with Separators
     * GUI.Separator()
     * GUI.Columns(3, "MyIdentifier", true);
     * GUI.Text("Text1"); GUI.NextColumn();
     * GUI.Text("Text2"); GUI.NextColumn();
     * GUI.Text("Text3"); GUI.NextColumn();
     * GUI.Separator()
     */
    export function NextColumn(): void;
    export function SliderFloat(): void;
    export function LabelText(): void;
    export function SetWindowCollapsed(): void;
    export function EndMenuBar(): void;
    /** Adds text to the GUI Context
     * @param {string} text - The text to show
     * @example GUI.Text("MyText");
     */
    export function Text(text: string): void;
    /** Adds a text input field to the current GUI context
     * @param {string} identifier - The identifier of the text field
     * @param {string} value - The default value
     * @example GUI.InputText("MyIdentifier", "MyDefaultValue");
     * @tupleReturn
     */
    export function InputText(identifier: string, value: string): [string, boolean]
    export function RadioButton(): void;
    /** Get the height of the current window in the GUI context
     * @returns The height of the current window
     * @example
     * const height = GUI.GetWindowHeight();
     * MyWindow.GUI.height = height;
     */
    export function GetWindowHeight(): number;
    export function ColorEdit4(): void;
    export function GetScreenSize(): void;
    export function IsItemFocused(): void;
    export function SliderInt3(): void;
    export function AddImage(): void;
    export function SetCursorPosY(): void;
    export function InputFloat2(): void;
    export function CalcItemWidth(): void;
    export function EndChildFrame(): void;
    export function AddQuadFilled(): void;
    export function PushButtonRepeat(): void;
    /** Get the width of the current window in the GUI context
     * @returns The width of the current window
     * @example
     * const width = GUI.GetWindowWidth();
     * MyWindow.GUI.width = width;
     */
    export function GetWindowWidth(): number;
    export function GetWindowFontSize(): void;
    export function SetWindowFontSize(): void;
    export function CalcItemRectClosestPoint(): void;
    export function SetKeyboardFocusHere(): void;
    export function SetItemAllowOverlap(): void;
    export function InputFloat3(): void;
    export function ColorConvertFloat4ToU32(): void;
    export function Image(): void;
    export function PopID(): void;
    export function EndMenu(): void;
    /** Creates a button within the current GUI context
     * @param {string} text - The text to be displayed on the button
     * @param {number} width - The width of the button
     * @param {number} height - The height of the Button
     * @returns {boolean} - returns true when clicked, otherwise returns false
     * @example 
     * if (GUI.Button("Click Me", 75, 15)) { 
     *      do something when the button is clicked 
     * }
     */
    export function Button(text: string, width: number, height: number): boolean;
    export function LogFinish(): void;
    export function GetScrollX(): void;
    export function AddCircleFilled(): void;
    export function SetNextWindowContentWidth(width: number, condition: number): void;
    export function BeginPopupContextWindow(): void;
    export function SetScrollX(): void;
    export function SetScrollFromPosY(): void;
    export function SetCursorPos(): void;
    export function GetCursorScreenPos(): void;
    export function GetFrameHeight(): void;
    export function OpenPopup(): void;
    export function PopTextWrapPos(): void;
    export function SetTooltip(label: string): void;
    /**
     * Check if the current window is hovered over
     * @param hoveredFlags The HoveredFlags integer to check on the window
     * @returns {boolean} Whether or not the window is hovered with the flags provided
     * @example
     * if (GUI.IsWindowHovered(GUI.HoveredFlags_AnyWindow)) {
     *      ... do stuff
     * }    
     */
    export function IsWindowHovered(hoveredFlags: number): boolean;
    export function CheckboxFlags(): void;
    export function BeginPopupContextItem(): void;
    /**
     * Set the next window to {collapsed}
     * @param collapsed Whether or not the window should be collapsed
     * @param flags WindowFlags to set
     * @example
     * GUI.SetNextWindowCollapsed(true, GUI.WindowFlags_FirstUseEver);
     * GUI.Begin("MyCollapsedWindow", MyCollapsedWindow.GUI.Open);
     * ... do stuff
     * GUI.End();
     */
    export function SetNextWindowCollapsed(collapsed: boolean, flags?: number): void;
    export function GetID(id: string): void;
    export function ColorConvertU32ToFloat4(): void;
    export function Combo(): void;
    export function LogText(): void;
    export function SliderFloat4(): void;
    export function GetItemRectMax(): void;

    /** Begin the GUI section where all contextual GUI functions will be executed
     * 
     * @param {string} name The unique name of the GUI context
     * @param {boolean} isOpen Whether or not the GUI should be open
     * @param {number} flags The integer representation of the flags you want on the GUI.
     * @returns {[boolean, boolean]} [0] - true if the GUI is not collapsed. [1] - Whether or not the GUI is Open (Frame is rendered)
     * @tupleReturn
     */
    export function Begin(name: string, isOpen: boolean, flags?: number): [boolean, boolean];
    export function GetGlobalFontSize(): void;
    export function Dummy(): void;
    export function PopAllowKeyboardFocus(): void;
    export function LogToFile(): void;
    export function BeginMenuBar(): void;
    export function IsRootWindowFocused(): void;
    export function GetCursorStartPos(): void;
    export function ShowStyleEditor(): void;
    export function SetNextWindowPosCenter(condition: number): void;
    /** Set the number of columns to be created within this GUI context
     * @param {number} numColumns - The number of columns to create
     * @param {string} identifier - The identifierof the column
     * @param {boolean} TODObool - TODO: Find out what this does, it is only set when an identifier is also set in my experience
     * @example
     * GUI.Columns(3, "MyIdentifier", true);
     * GUI.Text("Text1"); GUI.NextColumn();
     * GUI.Text("Text2"); GUI.NextColumn();
     * GUI.Text("Text3"); GUI.NextColumn();
     * @tupleReturn
     */
    export function Columns(numColumns: number, identifier?: string, TODObool?: boolean): void;
    export function GetStyleColName(): void;
    export function CalcTextSize(): void;
    export function FreeImageButton(): void;
    /** 
     * Set the position of the next window before draw, this must be called **before** GUI.Begin for that particular context
     * @param x The x coordinate of the window to be set
     * @param y The y coordinate of the window to be set
     * @param conditionFlags SetCond flag integer
     * @param pivotX X is adjusted based on this alignment, scale 0 to 1 
     * @param pivotY Y is adjusted based on this alignment, scale 0 to 1
     * For example, pass [0.5, 0.5] to center the window on the position
     * @example
     * GUI.SetNextWindowPos(0, 0, GUI.SetCond_FirstUseEver);
     * const [vis, op] = GUI.Begin("MyWindow", myWindow.GUi.Open);
     * myWindow.GUI.visible = vis;
     * myWindow.GUI.Open = op;
     * if (myWindow.GUI.visible) {
     *      do stuff with the window visible.
     * }
     * 
     */
    export function SetNextWindowPos(x: number, y: number, conditionFlags?: number, pivotX?: number, pivotY?: number): void;
    /** Get the position of the current window 
     * @returns [0]: number, [1]: number
     * @example
     * const [wX, wY] = GUI.GetWindowPos();
     * MyWindow.GUI.x = wX;
     * MyWindow.GUI.y = wY;
     */
    export function GetWindowPos(): [number, number]
    export function GetMouseDragDelta(): void;
    export function AddTriangleFilled(): void;
    export function InputFloat(): void;
    /** Set the content size of the next window
     * 
     * @param x The x axis size, set to 0 to leave automatic.
     * @param y The y axis size, set to 0 to leave automatic.
     * One of these two value so need to be set above 0, the other will automatically adjust if left at 0. 
     * @example
     * GUI.SetNextWindowContentSize(400, 0)
     * GUI.Begin("MyColumnText", nil, GUI.WindowFlagWindowFlags_AlwaysAutoResize);
     * GUI.Columns(2);
     * GUI.Text("Column 0");
     * GUI.Text("My Column Text");
     * GUI.NextColumn();
     * GUI.Text("Column 1");
     * GUI.Text("My Column second Text");
     * GUI.Columns(1);
     * GUI.End();
     */
    export function SetNextWindowContentSize(x: number, y: number): void;
    /** Get the available width available within the current content region
     * 
     * @returns The width of the content region available for use (Not occupied)
     * @example
     * const contentWidth = GUI.GetContentRegionAvailWidth();
     * GUI.SameLine(ContentWidth - 20);
     * GUI.Text("Right Aligned Text");
     */
    export function GetContentRegionAvailWidth(): number;
    export function IsItemHoveredRect(): void;
    export function GetItemsLineHeightWithSpacing(): void;
    export function ColorEditMode(): void;
    export function GetScrollY(): void;
    export function IsAnyItemFocused(): void;
    export function SetGlobalFontSize(): void;
    export function SetClipboardText(): void;
    export function GetClipboardText(): void;
    /** Creates a multi line input text field
     * 
     * @param identifier The identifier for this text field
     * @param text The text to be shown in the field on draw
     * @param sizeX The width of the input field
     * @param sizeY The height of the input field
     * @param flags The InputTextFlags int
     * @returns [0] The string currently in the text field [1] Whether or not this value has changed from origin
     * @example
     * const [val, changed] = GUI.InputTextMultiline("MyInputTextMultiLine", "");
     * if (changed) {
     *      d(val);
     * }
     * @tupleReturn
     */
    export function InputTextMultiline(identifier: string, text: string, sizeX?: number, sizeY?: number, flags?: number): [string, boolean];
    export function ColorConvertHSVtoRGB(): void;
    export function PopButtonRepeat(): void;
    export function GetItemRectMin(): void;
    /** Create a checkbox in the current GUI context
     * @param id 
     * @param value 
     * @tupleReturn
     */
    export function Checkbox(id: string, value: any): [boolean, boolean];
    export function DragFloat(): void;
    /** Checks to see if the current Window context is collapsed
     * 
     * @returns {boolean} Whether or not the current window context is collapsed on the screen
     * @example 
     * if (GUI.IsWindowCollapsed()) {
     *      .. do stuff
     * }
     */
    export function IsWindowCollapsed(): void;
    export function InvisibleButton(): void;
    export function FreeButton(): void;
    export function VSliderInt(): void;
    export function SetWindowFocus(): void;
    export function SliderInt(): void;
    export function TextWrapped(): void;
    export function CalcListClipping(): void;
    export function GetCursorPosY(): void;
    export function SliderFloat3(): void;
    export function IsMouseHoveringWindow(): void;
    export function SliderAngle(): void;
    export function AddRect(): void;
}
declare namespace ffxivminion {
    export function GetSetting(this: void, settingName: string, defaultValue?: number | string | boolean): number | string | boolean;
}
declare namespace Settings {
    export const FFXIVMINION: { [id: string]: number | string | boolean };
}
declare function GetControls(this: void): { [id: number]: UIControl };
declare class UIControl {
    // General
    public name: string;
    public ptr: number;


    // Methods
    public IsOpen(): boolean;
    // TODO:
    public GetActions(): { [id: number]: string };
    public GetData(): any;
    public Open(): any;
    public Close(): any;
    public Destroy(): any;
    public GetRawData(): {
        [idx: number]: {
            type: string,
            value: number | string | boolean
        }
    }
    /**
     * @tupleReturn
     */
    public GetXY(): [number, number];
    public SetXY(x: number, y: number): void;
    public Action(name: string): any;

}
//#endregion

//#region Events
declare type TMinionEvent = "Module.Initalize" | "Gameloop.Update" | "Gameloop.Draw" | "Game.UIEvent"
declare function RegisterEventHandler(this: void, event: TMinionEvent, callback: Function, description: string): void;
//#endregion

//#region Utility
// TODO: Complete this and move it.
declare interface INavigationPosition {

}

/**
 * @param {INavigationPosition} posTable A navigation position name from the Navigation Manager
 * @returns {number} The total path distance
 * @example PathDistance(NavigationManager.GetPath(myPos.x,myPos.y,myPos.z,p.x,p.y,p.z))
 */
declare function PathDistance(posTable: INavigationPosition): number
/** Returns the active game state enum
 * @returns {FFXIV.GAMESTATE} The game state from the FFXIV.GAMESTATE enum
 * @example 
 * if (GetGameState() == FFXIV.GAMESTATE.INGAME) { 
 *      .... 
 * }
 */
declare function GetGameState(this: void): FFXIV.GAMESTATE;
/** Returns the time between pulses with float precision.
 * @returns {number} The time between pulses @ float precision
 * @example d(GetBotPerformance)
 */
declare function GetBotPerformance(this: void): number;

/** Convert world coordinates to map coordinates
 * @param {number} localMapId - The local map ID for interpolation
 * @param {number} worldX - The X world coordinate
 * @param {number} worldY - The Y world coordinate
 * @param {number} worldZ - The Z world coordinate
 * @returns {[number, number, number]} [x, y, z] of the current map, converted from the world coordinates.
 * @example local mapX, mapY, mapZ = WorldToMapCoords(Player.localmapid, Player.pos.x, Player.pos.y, Player.pos.z)
 * @tupleReturn
 */
declare function WorldToMapCoords(this: void, localMapId: number, worldX: number, worldY: number, worldZ: number): [number, number, number];

declare function HasBuff(this: void, unit: GameObject, id: number): boolean;
/** Get the current map name
 * @returns {string} - The current map name
 * @example d(GetMapName())
 */
declare function GetMapName(this: void): string;
/** Prints to the console
 * @param {string[]} content - A string of items, using spread operator
 * @returns {void}
 * @example d('My Name Is', 'QT');
 */
declare function d(this: void, ...content: any[]): void;
/**
     *    Prints out the current call stack into the console.
     */
declare function stacktrace(): void
// TODO: Update Docs
/**
 *    Closes the current game instance
 */
declare function Exit(): void
/**
 *    Prints our the passed variable or the result of a function into the console when gEnableLog == “1”.
 */
declare function ml_debug(this: void, str: string): void;
/**
 *    Prints our the passed variable or the result of a function into the console.
 */
declare function ml_error(this: void, str: string): void

/**
  *    Adds the string to the statusbar-line which gets shown on each pulse.
  */
declare function ml_log(this: void, str: string): void;

/**
  *    Returns bool , reloads all lua modules
  */
declare function Reload(): void

/**
  *    Returns integer ml_global_information.Now - previousTime
  */
declare function TimeSince(previousTime: number): number

/**
  *    Returns bool , tries to unload the bot
  */
declare function Unload(): void

/**
  *     Queues and Fires the Event with 1-n arguments. Eventname and arguments need to be strings.    
  *     Use RegisterEventHandler(“eventname”, handlerfunc), to register a lua function which will handle the fired event.
  *     Requires at least 1 argument, even if it's a blank string. (ex. QueueEvent(“some event”,“”))
  */
declare function QueueEvent(eventname: string, args: string, ...params: any[]): void;
/** Gets the path of the LuaMods folder in Minion
 * @returns {string} - The filepath of the LuaMods folder
 * @example const MyLuaModsPath = GetLuaModsPath();
 */
declare function GetLuaModsPath(this: void): string | undefined;

/** Gets the current time (Set by minion, only for persistence)
 * @returns {number} The time as number
 * @example const LastUpdate = Now();
 */
declare function Now(this: void): number;

/**
 * Returns and memoizees your current target
 * @example
 * const Target = MGetTarget();
 * if (Target) {
 *      ... do stuff
 * }
 */
declare function MGetTarget(this: void): GameObject | undefined;

/** Whether or not the player's in a busy state, e.g dialogue, casting, cutscene, etc
 * 
 * @example
 * if (!Busy()) {
 *      ... do stuff that you would want to do when you arent busy.     
 * }
 */
declare function Busy(this: void): boolean;

//#endregion


//#region Files
declare namespace persistence {
    /** Load a file and store the value using MinionAPI
     * @param {string} filePath The file path to be loaded
     * @returns {[object, string]} - The file content, and optionally the error if it cannot load the file.
     * @example
     * const [content, e] = persistence.load('MyFilePath);
     * if (content) {
     *      ... do stuff with the content
     * } elseif (e) {
     *      d(e);
     * }
     * @tupleReturn
     */
    export function load(this: void, filePath: string): [{ [key: string]: unknown }, string];
    /** Save a file using hte minion API.
     * @param {string} filePath The file path where the {content} should be stored
     * @param {string} content The content to be stored.
     * @example persistence.store("myCoolFile.txt", "very cool text");
     */
    export function store(this: void, filePath: string, content: string): void;
}
/** Checks if a folder exists at the specified path.
 * @param {string} path - The path to check
 * @returns {boolean} Whether or not the folder exists
 * @example if (FolderExists(MyLuaModsPath + "QtLib")) { ....
 */
declare function FolderExists(this: void, path: string): boolean

/** Checks if a file exists at the specified path.
 * @param {string} path - The path to check
 * @returns {boolean} Whether or not the file exists
 * @example 
 * if (FileExists(MyLuaModsPath + "QtLib.lua")) {
 *      Do something if the file exists
 * }
 */
declare function FileExists(this: void, path: string): boolean

/** Creates a folder if it does not exist, does NOT create prior directories in a long path!
 * @param {string} path The path to check
 * @example FolderCreate(MyLuaModsPath + "QtLib");
 */
declare function FolderCreate(this: void, path: string): void

/** Loads a file into memory.
 * @param {string} path - The path to load.
 * @returns {any} The content of the file, however you have structured it (Use an explicit interface after loading for type checking)
 * @example const Settings = FileLoad(MySettingsPath);
 */
declare function FileLoad(this: void, path: string): any;

/** Saves a file to disk.
 * @param {string} path - The path to save.
 * @param {any} content - The content to save to file.
 * @example FileSave(MySettingsPath);
 */
declare function FileSave(this: void, path: string, content: any): void;

// TODO: Update Docs
/**
 *    Returns string, filepath to the root bot folder
 */
declare function GetStartupPath(): string

/**
     *    Returns bool , writes the string into the file.
     */
declare function FileWrite(fullpathtofile: string, data: string): boolean;

/**
 *    Returns bool , writes the string into the file. If 3rd arg is true, it will add the data to the end of the file.
 */
declare function FileWrite(fullpathtofile: string, data: string, append: boolean): boolean;

/**
 *    Returns bool
 */
declare function FileDelete(fullpathtofile: string): boolean

/**
 *    Returns bool
 */
declare function FileIsValidImage(fullpathtofile: string): boolean

/**
 *    Returns number
 */
declare function FileSize(fullpathtofile: string): number

/**
 *    Returns bool
 */
declare function FolderDelete(fullpathtofolder: string): boolean

/**
 *    Where pattern is normal regex and should be put in double brackets in lua. Eg. [[(.*)lua$]]    Returns table with all files in that directory.    
 *  defaults pattern = '', includeFolders = false
 */
declare function FolderList(fullpathtofolder: string, pattern: string, includeFolders: boolean): string[];
declare namespace table {
    export { _delete as delete };
    /** Checks if a table is valid with atleast 1 entry
     * 
     * @param {{}} table The table to be checked
     * @returns {boolean} Whether or not the table exists and has atleast 1 entry
     * @example 
     * if (table.valid(MyTable)) {
     *      ... do stuff
     * }
     */
    export function valid(this: void, table: {}): boolean;
    /** Sets all values in the table to nil
     * 
     * @param {{}} table The table to be cleared
     * @example table.clear(MyTable);
     */
    export function clear(this: void, table: {}): void;

    // TODO: Update Docs
    /**
      *    Returns bool, if arg2 was found in arg
      */
    export function contains(this: void, arg: string, arg2: string): void;

    /**
      *    Returns bool, if arg is type(string) and length is 0
      */
    export function empty(this: void, arg: string): boolean;

    /**
      *    Returns bool, if arg ends with arg2
      */
    export function ends(this: void, arg: string, arg2: string): boolean;

    /**
      *    Returns iterator, arg:  seperates, Usage: for moochable in string.split(moochables, ”,”) do … end
      */
    export function split(this: void, arg: string, seperator: string): Iterable<string>;

    /**
      *    Returns bool, if arg starts with arg2
      */
    export function starts(this: void, arg: string, arg2: string): boolean

    /**
      *    Returns bool
      */
    export function toboolean(this: void, arg: string): boolean

    /**
      *    Returns table, seperates arg
      */
    export function totable(this: void, arg: string, seperator: string): string[];

    /**
      *    Returns bool, trims arg by num characters
      */
    export function trim(this: void, arg: string, num: number): boolean;

    /**
      *    Returns bool, if arg is type(string)
      */
    export function valid(this: void, arg: string): boolean;

    /**
      *    Returns bool, if the str begins with “starts”
      */
    export function starts(this: void, str: string, starts: string): boolean;

    /**
      *    Returns bool, if the str ends with “ends”
      */
    export function ends(this: void, str: string, ends: string): boolean;

    /**
      *    Returns number, a md5 encoded number for the passed string
      */
    export function hash(this: void, arg: string): number;
    /**
      *    Returns bool if the value exists in the table
      */
    export function contains(this: void, table: {} | [], value: any): boolean;

    /**
      *    Returns table, makes a deepcopy of the passed table. Pass “true” as 2nd arg to not duplicate the metatable.
      */
    export function deepcopy<T>(this: void, arg: T, skipMetaTable: boolean): T;

    /**
      *    Returns bool, compares two tables if they are equal.
      */
    export function deepcompare(this: void, table1: {} | [], table2: {} | [], ignore_metatable: boolean): boolean

    /**
      *    Returns bool if the object was removed from the table
      */
    export function _delete(this: void, table: {} | [], object: {}): boolean

    /**
      *    Returns number if the value exists in the table, position:  it returns the key, else nil
      */
    export function find(this: void, table: {} | [], value: any): number;

    /**
      *    Returns table, flips a table so keys become values
      */
    export function invert<T>(this: void, arg: T): T

    /**
      *    Returns table1, merged with table 2. If keepexistingentries is not passed, all existing keys&values in table1 will be overwritten with the ones from table2, else the values of table2 are just inserted into table1.
      */
    export function merge<T>(this: void, table1: T, table2: T, keepexistingentries: boolean): T;

    /**
      *    Returns iterator over the table which is default sorted by its keys. Optionally pass your custom sort function as 2nd arg. Usage: “for key,value in table.pairsbykeys(table) do..”
      */
    export function pairsbykeys(this: void, table1: {} | [], sort: () => number): Iterator<{} | []>

    /**
      *    Returns iterator over the table which is default sorted by its values. Optionally pass your custom sort function as 2nd arg. Usage: “for key,value in table.pairsbyvalue(table) do..”
      */
    export function pairsbyvalue(this: void, table1: {} | [], sort: () => number): Iterator<{} | []>

    /**
      *    Prints the table content line by line into the minion console.
      */
    export function print(this: void, arg: {} | []): void

    /**
      *    Returns a random value from the table.
      */
    export function randomvalue(this: void, arg: {} | []): any;

    /**
      *    Returns table
      */
    export function shallowcopy<T>(this: void, arg: T): T;

    /**
      *    Returns number, of elements the table contains. Returns 0 if the arg is no table.
      */
    export function size(this: void, arg: {} | []): number
}

//#endregion


//#region Powershell
declare namespace io {
    // Powershell string execute
    export function popen(this: void, command: string): void;
}

//#endregion

//#region Units
declare namespace EntityList {
    export const crossworldparty: PartyMember[];
    export const myparty: PartyMember[];
    export function Get(id: number): GameObject | undefined;
}
/**
 *  @param {string} filter Comma seperated string of filters
 *  maxdistance=number 
 *  Returns Entities within a distance of "number".
 * 
 *  mindistance=number
 *  Returns Entities beyond a distance of "number".
 * 
 *  nearest
 *  Returns the closest Entity.
 * 
 *  targetingme
 *  Returns Entities which are targeting the player.
 * 
 *  targeting=number
 *  Returns Entities which target the entity with the ID "number".
 * 
 *  los
 *  Returns Entities within line of sight to the player.
 * 
 *  alive
 *  Returns Entities with health > 0.
 * 
 *  dead
 *  Returns Entities with health == 0.
 * 
 *  aggressive
 *  Returns aggressive Entities
 * 
 *  friendly
 *  Returns friendly Entities
 * 
 *  incombat
 *  Returns Entities which are incombat
 * 
 *  notincombat
 *  Returns Entities which are not in combat
 * 
 *  onmesh
 *  Returns Entities which are on the navigation mesh.
 * 
 *  gatherable
 *  Returns Entities which you can gather or harvest.
 * 
 *  minlevel=number
 *  Returns Entities with a level higher than "number".
 * 
 *  maxlevel=number
 *  Returns Entities with a level lower than "number".
 * 
 *  lowesthealth
 *  Returns the Entity with the lowest health.
 * 
 *  shortestpath
 *  Returns the Entity with the shortest path (navigation path, not direct distance!).
 * 
 *  type=number
 *  Returns Entities with the enum ENTITYTYPE "number"
 * 
 *  chartype=number
 *  Returns Entities with the enum CHARACTERTYPE"number"
 * 
 *  contentid=number
 *  Returns Entities which have the given contentID, this can be used to search for specific NPCs/Enemies/Targets. Has been changed to also accept a list of content id's separated by a semicolon.
 *  
 *  exclude_contentid=number
 *  Returns Entities which do not have the given contentID, this can be used to exclude specific NPCs/Enemies/Targets. Also accept a list of content id's separated by a semicolon.
 * 
 *  exclude=number
 *  Returns Entities without the one with the entityID "number". Has been changed to also accept a list of entity id's separated by a semicolon.
 * 
 *  aggro
 *  Returns Entities which are having aggro towards the player.
 * 
 *  attackable
 *  Returns Entities which you can attack.
 * 
 *  ownerid=number
 *  Returns the Entity which has an owner with the ID "number". This can be used to get for ex. the Pet from the Player
 * 
 *  fateid=number
 *  Returns Entities which belong to the fate with the ID "number".
 * 
 *  distanceto=number
 *  Calculates every distance-filter towards the entityID passed as "number" instead of the player.
 * 
 *  clustered=number
 *  Returns the enemy who has the most enemies around him up to a distance of "number", use this to get the best AOE target.
 * 
 *  myparty
 *  Returns Entities which belong to your party. Works only when they are close enough and on the same map
 */
declare function EntityList(this: void, filter: string): { [id: string]: GameObject } | undefined;
declare interface IBuff {
    ptr: number;
    ptr2: number;
    id: number;
    duration: number;
    name: string;
    ownerid: number;
    isbuff: boolean;
    isdebuff: boolean;
    stacks: number;
    slot: number;
    dispellable: boolean;
}
// Probably not the one you want, buf this is what it is called in the framework
declare interface IBuff2 {
    duration: number,
    id: number,
    name: string,
    ownerid: number
}
declare class Entity {
    public ptr: number;
    public id: number;
    public type: number;
    public pos: I3DCoordinate;
}
declare interface IPowerBar {
    current: number;
    max: number;
    percent: number
}
declare class GameObject {
    // Core Data
    public ptr: number;
    public id: number;
    public name: string;
    public contentid: number;
    public type: number;
    //TODO: very likely has a strict enum, need to find it
    public status: number;
    //TODO: very likely has a strict enum, need to find it
    public chocobostate: number;
    public chartype: number;
    public targetid: number;
    public ownerid: number;
    public claimedbyid: number;
    public fateid: number;
    public iconid: number;
    // Bars Data
    public hp: IPowerBar;
    public mp: IPowerBar;
    public cp: IPowerBar;
    public gp: IPowerBar;
    public tp: number;
    // Position 
    public pos: I3DCoordinate;
    public hitradius: number;
    public distance: number;
    public distance2d: number;
    public pathdistance: number;
    public los: boolean;
    public los2: boolean;
    public onmesh: boolean;
    public isreachable: boolean;
    public meshpos: IMeshPosition;
    public cubepos: IMeshPosition;
    // Misc Data
    public ismounted: boolean;
    public job: number;
    public level: number;
    // Enum?
    public pvpteam: number;
    public grandcompany: 1 | 2 | 3;
    public grandcompanyrank: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19;
    public aggro: boolean;
    public aggropercentage: number;
    public hasaggro: boolean;
    public revivestate: 0 | 1;
    // Enum?
    public role: number;

    public attackable: boolean;
    public aggressive: boolean;
    public friendly: boolean;
    public incombat: boolean;
    public interactable: boolean;
    public targetable: boolean;
    public alive: boolean;
    public cangather: boolean;
    // enums??
    public spearfishstate: number;
    public marker: number;
    public onlinestatus: number;
    public currentworld: number;
    public homeworld: number
    // Casts and Spell Data;
    public action: number;
    public lastaction: number;
    public castinginfo?: ICastingInfo;
    // Eureka (the fuck is that)
    public eurekainfo?: {
        level: number;
        element: number;
    }
    // Buffs
    // 
    public buffs?: { [index: number]: IBuff }


}
declare class PartyMember extends GameObject {
    public partyindex: number;
    public isonline: boolean;
    public guid: string;
    public iscrossworld: boolean;
    public isleader: boolean;
}
//#region Player
declare type TJob = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 | 37 | 38 | 39 | 40;
declare interface IGearset {
    job: TJob,
    name: string
}
declare class PlayerUnit extends GameObject {
    // General
    public hpp: number;
    public mpp: number;
    public buffs2: IBuff2;
    // Maps
    public localmapid: string;
    // Gauges
    public gauge: { [index: number]: number };
    public gaugetest: { [index: number]: number };
    // Job Levels
    public levels: { [index: number]: number }[];
    // Stats & Attrs
    public stats: { [index: number]: number }[];
    // Settings (Not sure why this is in player)
    public settings: {
        autoface: boolean,
        movemode: 0 | 1
    }
    // Cast stuff
    public combotimeremain: number;
    public lastcomboid: number;


    // Prototype?
    public __object: any;
    public BuildPath(): void;
    public ClearTarget(): void;
    public Dive(): void;
    public FollowTarget(): void;
    public Gather(): void;
    public GetAetherCurrentsList(): void;
    public GetAetheryteList(): void;
    public GetBait(): void;
    public GetChocoboRacingInfo(): void;
    public GetDeepDungeonInfo(): void;
    public GetEnmityList(): void;
    public GetFishingState(): void;
    public GetGatherableSlotList(): void;
    public GetGearSetList(): IGearset;
    public GetGigHead(): void;
    public GetGSCardList(): void;
    public GetGSDeckList(): void;
    public GetLastCatchId(): void;
    public GetSnipeCam(): void;
    public GetSnipeTargetsRemain(): void;
    public GetSpeed(): void;
    // 100 stats, allegedly
    public GetStats(index: number): number;
    public GetSyncLevel(): void;
    public GetTarget(): TargetUnit | undefined;
    public GetTripleTriadInfo(): void;
    public Interact(): void;
    public Invite(): void;
    public IsAutoFollowOn(): void;
    public IsJumping(): void;
    public IsMoving(): void;
    public IsWalking(): void;
    public Jump(): void;
    public KickPartyMember(): void;
    public Move(): void;
    public MoveTo(): void;
    public MoveToStraight(): void;
    public PauseMovement(): void;
    public ResetSpeed(): void;
    public Ride(): void;
    public SendChocoboRacingMoveEvent(): void;
    public SetAutoFace(): void;
    public SetAutoFollowOn(): void;
    public SetAutoFollowPos(): void;
    public SetBait(): void;
    public SetFacing(): void;
    public SetGSDeckCard(): void;
    public SetMoveMode(): void;
    public SetPitch(): void;
    public SetRunMode(): void;
    public SetSnipeCam(): void;
    public SetSpeed(): void;
    public SetTarget(): void;
    public SetWalkMode(): void;
    public SnipeHasTarget(): void;
    public SnipeShoot(): void;
    public Stop(): void;
    public StopMovement(): void;
    public SyncLevel(): void;
    public TakeOff(): void;
    public Teleport(): void;
    public UseDeepDungeonItem(): void;
    public UseDeepDungeonMagicite(): void;
}
declare class TargetUnit extends GameObject { }
//#endregion
//#endregion


//#region Actions

declare class BaseAction {
    public ptr: number;
    public id: number;
    public type: FFXIV.ACTIONTYPE;
    public skilltype: number;
    public cost: number;
    public casttime: number;
    public recasttime: number;
    public isoncd: boolean;
    // How long it has been on cd, not the remaining cd
    public cd: number;
    public cdmax: number;
    public range: number;
    public radius: number;
    public level: number;
    public job: TJob;
    public iscasting: boolean;
    public combospellid: number;
    public isgroundtargeted: boolean;
    public usable: boolean;
    public isready: boolean;
    public Cast(id: number): boolean;
    public IsReady(): boolean;
    public IsFacing(): boolean;
    // TODO: What is this 
    public CanCastResult(): any;
}

declare class PlayerAction extends BaseAction {
    public attacktype: number;
    public cooldowngroup: number;
    public statusgainedid: number;
    public secondarycostid: number;
    public aspect: number;
    public category: number;
    public primarycosttype: number;
}
declare class MountAction extends BaseAction {
    public canfly: boolean;
}
declare class MinionActionList {
    // TODO: Has this changed?
    public Get(type: FFXIV.ACTIONTYPE): { [actionId: number]: PlayerAction };
    public Get(type: FFXIV.ACTIONTYPE, targetId: number): PlayerAction;
    public Get(actionId: number, type: FFXIV.ACTIONTYPE, targetId: number): PlayerAction;
    public GetTypes(): FFXIV.ACTIONTYPE;
    public IsReady(): boolean;
    public StopCasting(): void;
    public IsCasting(): boolean;
}
//#endregion

//#region Items
declare interface Item {
    ptr: number;
    ptr2: number;
    name: string;
    id: number;
    ishq: boolean;
    hqid: number;
    category: FFXIV.ITEMCATEGORY;
    slot: number;
    type: FFXIV.ITEMCATEGORY;
    condition: number;
    count: number;
    max: number;
    isready: boolean;
    collectability: number;
    requiredlevel: number;
    class: number;
    uicategory: number;
    searchcategory: number;
    canequip: boolean;
    equipslot: number;
    price: number;
    materiaslotcount: number;
    materias: { [id: number]: IMateria };
    rarity: number;
    isunique: boolean;
    isuntradeable: boolean;
    iscollectable: boolean;
    desynthvalue: number;
    repairclassjob: number;
    repairitem: number;
    isbinding: boolean;
    spiritbond: number;
    Use(): void;
    Use(entityId: number): void;
    Repair(): void;
    Discard(): void;
    Split(): void;
    HandOver(): void;
    SelectCraft(): void;
    Convert(): void;
    GetAction(): void;
    LowerQuality(): void;
    oCast(): void;
    IsReady(): void;
    Salvage(): void;
    GetStat(): void;
    Move(): void;
    SelectFeed(): void;
    Gardening(): void;
    __object(): void;
    Cast(): void;
    CanCast(): void;
    Transmute(): void;
    Meld(): void;
    Sell(): void;
    Purify(): void;
    RetrieveMateria(): void;
}
declare interface IMateria {
    itemid: number;
    name: string;
    attribute: string;
    value: number;
}
declare interface IInventory {
    __object(): void;
    RepairAll(): void;
    GetItem(): void;
    GetList(): Item[];
    GetSortedItemList(): void;
    Get(): void;
}
declare namespace Inventory {
    export function Get(filterOrItemId: number): IInventory | undefined;
    export function MaxSlots(): number
    export function FreeSlots(): number;
    export function UsedSlots(): number;
    export function GetItemExchangeList(): void;
    export function GetSpecialCurrencies(): void;
    export function SearchForItem(): void;
    export function GetLootList(): void;
    export function GetCurrencyCountByID(): void;
    export function GetShopList(): void;
    export function BuyShopItem(): void;
    export function GetTypes(): void;
    export function SortInventory(): void;
    export function GetItemDetails(): void;
}
/** "type=1,2,3,4,itemid=123,category=1"
 * type = FFXIV.INVENTORYTYPE
 * itemid = number
 * category = FFXIV.ITEMCATEGORY
 */
declare function Inventory(filter: string): Item[] | undefined;
//#endregion