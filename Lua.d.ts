declare function tostring(this: void, item: any): string;
declare function loadstring(this: void, str: string): boolean;
declare const _G: any;

declare namespace math {
    /**
     * Returns the absolute value of a number (the value without regard to whether it is positive or negative).
     * For example, the absolute value of -5 is the same as the absolute value of 5.
     * @param x A numeric expression for which the absolute value is needed.
     */
    export function abs(this: void, x: number): number;
    /**
     * Returns the arc cosine (or inverse cosine) of a number.
     * @param x A numeric expression.
     */
    export function acos(this: void, x: number): number;
    /**
     * Returns the arcsine of a number.
     * @param x A numeric expression.
     */
    export function asin(this: void, x: number): number;
    /**
     * Returns the arctangent of a number.
     * @param x A numeric expression for which the arctangent is needed.
     */
    export function atan(this: void, x: number): number;
    /**
     * Returns the angle (in radians) from the X axis to a point.
     * @param y A numeric expression representing the cartesian y-coordinate.
     * @param x A numeric expression representing the cartesian x-coordinate.
     */
    export function atan2(this: void, y: number, x: number): number;
    /**
     * Returns the smallest integer greater than or equal to its numeric argument.
     * @param x A numeric expression.
     */
    export function ceil(this: void, x: number): number;
    /**
     * Returns the cosine of a number.
     * @param x A numeric expression that contains an angle measured in radians.
     */
    export function cos(this: void, x: number): number;
    /**
     * Returns e (the base of natural logarithms) raised to a power.
     * @param x A numeric expression representing the power of e.
     */
    export function exp(this: void, x: number): number;
    /**
     * Returns the greatest integer less than or equal to its numeric argument.
     * @param x A numeric expression.
     */
    export function floor(this: void, x: number): number;
    /**
     * Returns the natural logarithm (base e) of a number.
     * @param x A numeric expression.
     */
    export function log(this: void, x: number): number;
    /**
     * Returns the larger of a set of supplied numeric expressions.
     * @param values Numeric expressions to be evaluated.
     */
    export function max(this: void, ...values: number[]): number;
    /**
     * Returns the smaller of a set of supplied numeric expressions.
     * @param values Numeric expressions to be evaluated.
     */
    export function min(this: void, ...values: number[]): number;
    /**
     * Returns the value of a base expression taken to a specified power.
     * @param x The base value of the expression.
     * @param y The exponent value of the expression.
     */
    export function pow(this: void, x: number, y: number): number;
    /** Returns a pseudorandom number between 0 and 1. */
    export function random(): number;
    /**
     * Returns a supplied numeric expression rounded to the nearest integer.
     * @param x The value to be rounded to the nearest integer.
     */
    export function round(this: void, x: number): number;
    /**
     * Returns the sine of a number.
     * @param x A numeric expression that contains an angle measured in radians.
     */
    export function sin(this: void, x: number): number;
    /**
     * Returns the square root of a number.
     * @param x A numeric expression.
     */
    export function sqrt(this: void, x: number): number;
    /**
     * Returns the tangent of a number.
     * @param x A numeric expression that contains an angle measured in radians.
     */
    export function tan(this: void, x: number): number;

    /**
      *    Returns number, calculates the shortest angle between two headings (x:  2x table with,y, : z Result is angle between 0 - 180 degree
      */
    export function angle(this: void, heading1: I3DCoordinate, heading2: I3DCoordinate): number;

    /**
      *    Returns bool
      */
    export function approxequal(this: void, num1: number, num2: number): boolean

    /**
      *    Returns table
      */
    export function crossproduct(this: void, pos1: I3DCoordinate, pos2: I3DCoordinate): I3DCoordinate;

    /**
      *    Returns number
      */
    export function distance2d(this: void, x: number, y: number, x1: number, y1: number): number;

    /**
      *    Returns number
      */
    export function distance3d(this: void, pos1: I3DCoordinate, pos2: I3DCoordinate): number;

    /**
      *    Returns number
      */
    export function distance3d(this: void, x: number, y: number, z: number, x1: number, y1: number, z1: number): number;

    /**
      *    Returns number, points:  takes in 3, first two are the “line” defining points, last one is the point we want to get the distance to that line from
      */
    export function distancepointline(this: void, p1: I3DCoordinate, p2: I3DCoordinate, p3: I3DCoordinate): number;

    /**
      *    Returns number
      */
    export function magnitude(this: void, pos: I3DCoordinate): number;

    /**
      *    Returns number
      */
    export function round(this: void, num: number, decimals: number): number;

    /**
      *    Returns integer , takes in a percentage from 0-100 and gives back a random number “near” that value
      */
    export function randomize(this: void, int: number): number
}